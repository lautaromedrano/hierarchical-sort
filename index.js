const fs = require('fs');

const TOTAL = '$total';

/**
 * Count how many property fields we have.
 *
 * @param {*} headers
 * @returns
 */
function countProperties(headers) {
    let propertiesCount = 0;

    headers.forEach((header) => {
        if (header.indexOf('property') === 0) {
            propertiesCount++;
        }
    });

    return propertiesCount;
}

/**
 * Convert an array to a JS object based on the defined headers.
 *
 * @param {*} headers
 * @param {*} row
 * @returns
 */
const getRowObject = (headers, row) => {
    let rowObject = {};

    for (let i = 0; i < row.length; i++) {
        rowObject[headers[i]] = row[i];
    }

    return rowObject;
};

/**
 * Check if a row is the summary of a category or subcategory.
 *
 * @param {*} propertiesCount
 * @param {*} rowObject
 * @returns
 */
const isTotalRow = (propertiesCount, rowObject) => {
    for (let i = 0; i < propertiesCount; i++) {
        if (rowObject[`property${i}`] === TOTAL) {
            return true;
        }
    }

    return false;
};

/**
 * Get an identifier for a summary row.
 *
 * @param {*} propertiesCount
 * @param {*} rowObject
 * @returns
 */
const getTotalKeyForTotalRow = (propertiesCount, rowObject) => {
    let properties = [];

    for (let i = 0; i < propertiesCount && rowObject[`property${i}`] !== TOTAL; i++) {
        properties.push(rowObject[`property${i}`]);
    }

    properties.push(TOTAL);
    return properties.join('_');
};

/**
 * Get the corresponding total key for a value row.
 *
 * @param {*} propertiesCount
 * @param {*} rowObject
 * @returns
 */
const getTotalKeyForValueRow = (propertiesCount, rowObject) => {
    let properties = [];

    for (let i = 0; i < propertiesCount + 1; i++) {
        properties.push(rowObject[`property${i}`]);
    }

    properties.push(TOTAL);
    return properties.join('_');
};

/**
 * Parse once to get all summary rows in a map
 *
 * @param {*} lines
 * @param {*} headers
 * @param {*} propertiesCount
 * @param {*} totals
 * @param {*} getSortValueFn
 */
function calculateTotals(lines, headers, propertiesCount, totals, getSortValueFn) {
    lines.forEach(
        (line) => {
            let rowObject = getRowObject(headers, line.split('|'));
            let isTotal = isTotalRow(propertiesCount, rowObject);

            if (isTotal) {
                let key = getTotalKeyForTotalRow(propertiesCount, rowObject);
                totals[key] = getSortValueFn(rowObject);
            }
        }
    );
}

/**
 * Auxiliary function that defines when a row is bigger or smaller than another.
 *
 * @param {*} rowA
 * @param {*} rowB
 * @param {*} propertiesCount
 * @param {*} totals
 * @param {*} getSortValueFn
 * @returns
 */
const compare = (rowA, rowB, propertiesCount, totals, getSortValueFn) => {
    for (let i = 0; i < propertiesCount - 1; i++) {
        // If both are total, we are comparing categories/subcategories
        if (rowA[`property${i}`] === TOTAL && rowB[`property${i}`] === TOTAL) {
            return getSortValueFn(rowB) - getSortValueFn(rowA);
        }

        // If only A or B is total, it's the summary row for the shared category/subcategory
        if (rowA[`property${i}`] === TOTAL) {
            return -1;
        }
        if (rowB[`property${i}`] === TOTAL) {
            return 1;
        }

        // If none is total, they belong to different categories/subcategories.
        // They must be compared through it's first different level
        if (rowA[`property${i}`] !== rowB[`property${i}`]) {
            return totals[getTotalKeyForValueRow(i, rowB)] - totals[getTotalKeyForValueRow(i, rowA)];
        }
    }

    // Check if the last property is total
    if (rowA[`property${propertiesCount - 1}`] === TOTAL) {
        return -1;
    }
    if (rowB[`property${propertiesCount - 1}`] === TOTAL) {
        return 1;
    }

    // If we get to this, they are same items from the same category/subcategory
    return getSortValueFn(rowB) - getSortValueFn(rowA);
};


const hierarchicalSort = (data, getSortValueFn) => {
    let lines = data.split('\n');
    let headers = lines.shift().split('|');

    let totals = {};
    let propertiesCount = countProperties(headers);

    calculateTotals(lines, headers, propertiesCount, totals, getSortValueFn);

    return lines.sort(
        (a, b) => compare(getRowObject(headers, a.split('|')), getRowObject(headers, b.split('|')), propertiesCount, totals, getSortValueFn)
    );
};

/**
 * Read file and sort hierarchically
 */
fs.readFile(process.argv[2], 'utf8', (err, data) => {
    if (err) {
        console.error(err);
        return;
    }
    let result = hierarchicalSort(data, (row) => row.net_sales);
    console.log(result);
});
